#!/bin/bash
sudo yum -y install gcc
sudo yum install -y python3-devel
sudo pip3 install -U \
    boto3 \
	s3fs
