# -*- coding: utf-8 -*-
"""
Created on Sun Feb 21 19:01:38 2021

@author: Marcelo Valverde Frasca
"""

# Importando libs do Spark
from pyspark.sql import Window, DataFrame,functions as F
from pyspark.sql import SparkSession
from pyspark.sql.types import *

# Definindo nomes e endereços de origem (CSV no S3) e destino (datalake)
SOURCE_BUCKET_NAME = 's3://bacen-if.data/'
DATA_SOURCE_NAME = 'dados.csv'
TARGET_BUCKET_NAME = 's3://itau-datalake/'

# Criando aplicação Spark
spark = SparkSession.builder.appName("Spark-Ingest-Bacen-IfData-Ativo").getOrCreate()

# Preparado o schema do data source de entrada (inicialmente com data types texto)
raw_schema = StructType([
    StructField("instituicao_financeira", StringType()),
    StructField("codigo", StringType()),
    StructField("conglomerado", StringType()),
    StructField("conglomerado_financeiro", StringType()),
    StructField("conglomerado_prudencial", StringType()),
    StructField("tipo_consolidado_bancario", StringType()),
    StructField("tipo_controle", StringType()),
    StructField("tipo_instituicao", StringType()),
    StructField("cidade", StringType()),
    StructField("uf", StringType()),
    StructField("data", StringType()),
    StructField("disponibilidades", StringType()),
    StructField("aplicacoes_interfinanceiras_liquidez", StringType()),
    StructField("tvm_instrumentos_financeiros_derivativos", StringType()),
    StructField("operacaoes_credito", StringType()),
    StructField("provisao_sobre_operacao_credito", StringType()),
    StructField("operacoes_credito_liquidas_provisao", StringType()),
    StructField("arrendamento_mercantil_a_receber", StringType()),
    StructField("imobilizado_arrendamento", StringType()),
    StructField("credores_antecipacao_valor_residual_arrendamento_mercantil", StringType()),
    StructField("provisao_sobre_arrendamento_mercantil_cl", StringType()),
    StructField("arrendamento_mercantil_liquido_provisao", StringType()),
    StructField("outros_creditos_liquido_provisao", StringType()),
    StructField("outros_ativos_realizaveis", StringType()),
    StructField("permanente_ajustado", StringType()),
    StructField("ativo_total_ajustado", StringType()),
    StructField("credores_antecipacao_valor_residual", StringType()),
    StructField("ativo_total", StringType()),
])

# Carregando fonte de dados CSV
data_source_path = SOURCE_BUCKET_NAME + 'ativos/' + DATA_SOURCE_NAME
df_ativos_csv = spark.read.csv(path=data_source_path, sep=';', header=True, schema=raw_schema)

# Gerando arquivo em formato colunar Parquet
parquet_name = 'ifdata-ativos.parquet'
parquet_target_path = TARGET_BUCKET_NAME + 'raw/' + parquet_name
df_ativos_csv.write.parquet(parquet_target_path, mode='overwrite')
# Libera dataframe da memória
df_ativos_csv.unpersist()

# Carregando fonte de dados Parquet recém criada
df_ativos_raw = spark.read.parquet(parquet_target_path)

# Removendo registros duplicados
df_ativos = df_ativos_raw.dropDuplicates()
df_ativos_raw.unpersist()

# Removendo registros com instituição financeira ou código nulos
'''
Tomei a liberdade de considerar a coluna código com  o intuito de remover também as linhas do final do 
arquivo que são totalizadores agrupados por TCB, TC e TI)
''' 
df_ativos = df_ativos.na.drop(subset=["instituicao_financeira","codigo"])

# Atualizando a tipagem dos dados após saneamento para o resultado final a ser carregado no datalake
df_ativos.createOrReplaceTempView("ativos")
df_ativos = spark.sql("""
                select instituicao_financeira,
                cast(codigo as int) as codigo,
                conglomerado,
                cast(conglomerado_financeiro as int) as conglomerado_financeiro,
                conglomerado_prudencial,
                tipo_consolidado_bancario,
                cast(tipo_controle as int) as tipo_controle,
                cast(tipo_instituicao as int) as tipo_instituicao,
                cidade,
                uf,
                data,
                cast(disponibilidades as double) as disponibilidades,
                cast(aplicacoes_interfinanceiras_liquidez as double) as aplicacoes_interfinanceiras_liquidez,
                cast(tvm_instrumentos_financeiros_derivativos as double) as tvm_instrumentos_financeiros_derivativos,
                cast(operacaoes_credito as double) as operacaoes_credito,
                cast(provisao_sobre_operacao_credito as double) as provisao_sobre_operacao_credito,
                cast(operacoes_credito_liquidas_provisao as double) as operacoes_credito_liquidas_provisao,
                cast(arrendamento_mercantil_a_receber as double) as arrendamento_mercantil_a_receber,
                cast(imobilizado_arrendamento as double) as imobilizado_arrendamento,
                cast(credores_antecipacao_valor_residual_arrendamento_mercantil as double) as credores_antecipacao_valor_residual_arrendamento_mercantil,
                cast(provisao_sobre_arrendamento_mercantil_cl as double) as provisao_sobre_arrendamento_mercantil_cl,
                cast(arrendamento_mercantil_liquido_provisao as double) as arrendamento_mercantil_liquido_provisao,
                cast(outros_creditos_liquido_provisao as double) as outros_creditos_liquido_provisao,
                cast(outros_ativos_realizaveis as double) as outros_ativos_realizaveis,
                cast(permanente_ajustado as double) as permanente_ajustado,
                cast(ativo_total_ajustado as double) as ativo_total_ajustado,
                cast(credores_antecipacao_valor_residual as double) as credores_antecipacao_valor_residual,
                cast(ativo_total as double) as ativo_total
                from ativos
                """)

# Gerando arquivo em formato colunar Parquet já com a tipagem atualizada
parquet_name = 'ifdata-ativos.parquet'
parquet_target_path = TARGET_BUCKET_NAME + 'structured/' + parquet_name
df_ativos.write.parquet(parquet_target_path, mode='overwrite')
# Libera dataframe da memória
df_ativos.unpersist()